"""
LoadDataS3.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

LoadDataS3 class has the functionality to fetch data from DynamoDB and load it in S3
"""

from platformutils.dynamodb_utils import DynamoDBUtils
from platformutils.utils.time_util import TimeUtil
from platformutils.utils.file_util import FileUtil
import json
from decimal import Decimal
from platformutils.s3_utils import S3Utils

class LoadDataS3(object):

    def __init__(self, build, region_name, level):
	trip_table = 'ppacn-agero-{:}-trip-event'.format(build)
	self.db = DynamoDBUtils(trip_table, region_name, level)
	self.time =TimeUtil()    
	self.file_util =FileUtil()
	self.s3 = S3Utils(region_name, level)
	self.current_directory = self.file_util.get_current_directory()
	self.bucket = 'policypalacnprod'

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
    	raise TypeError

	
    def get_data(self):
	"""
	This method is used to fetch data from DynamoDB.
	Upload the file to S3
        """
	path = self.current_directory + "/trip-event-metadata-" + str(self.time.get_date(1)) + ".json"
	utc_value = self.time.get_utc_timestamp(1, 'ms')
	response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index')
	while True:
	    for rows in response['Items']:
		row = json.dumps(rows, default=self.default)
		self.file_util.write_data(path, row+"\n")
	    if response.get('LastEvaluatedKey'):
	        response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index', last_evaluated_key = response['LastEvaluatedKey'])
	    else:
		break
	self.file_util.compress_text_file(path)	
	self.upload_data(path + ".gz")
	self.file_util.change_directory(self.current_directory)
	self.file_util.remove_file_start_with_name('trip-event-metadata-')

    def upload_data(self, path):
	"""
	This method is used to upload data into S3.
        :param path: Provide path has to be load the data.
        """
	bucket_key = 'agero/prod/datapipeline/dynamodb-backup/prod-trip-event/metadata/trip-event-metadata-' + str(self.time.get_date(1)) + '.json.gz'
	self.s3.upload_object(path, self.bucket, bucket_key)
