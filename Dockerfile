FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y python python-pip wget
RUN pip install --upgrade pip
RUN pip install boto3
RUN pip install psycopg2

COPY platformutils /platformutils
ADD handler.py /
ADD dashboard.py /
ADD load_data_s3.py /


CMD [ "python", "./handler.py"]




